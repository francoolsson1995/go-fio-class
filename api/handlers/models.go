package handlers

type userCoursesInfoRequest struct {
	UserID  string         `json:"userId"`
	Courses []dependencies `json:"courses"`
}

type dependencies struct {
	DesiredCourse  string `json:"desiredCourse"`
	RequiredCourse string `json:"requiredCourse"`
}

type orderedCoursesResponse struct {
	UserID      string `json:"userId"`
	Information []info `json:"courses"`
}

type info struct {
	Name  string `json:"course"`
	Order int    `json:"order"`
}
