package handlers

import (
	"github.com/gofiber/fiber/v2"
)

const (
	coursePathGroup = "/courses"
	sortPath        = "/sort"
)

// CoursesRoutes returns a fiber.App with the routes for the courses services.
func CoursesRoutes(app *fiber.App, service coursesService) *fiber.App {
	serviceHandler := NewCoursesHandler(service)
	courseGroup := app.Group(coursePathGroup)
	courseGroup.Post(sortPath, serviceHandler.GetOrderedCourses)
	return app
}
