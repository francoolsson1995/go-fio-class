package main

import (
	"os"

	"fio-gin-context/cmd/api/handlers"
	"fio-gin-context/internal/course"
	"fio-gin-context/internal/platform/sort"

	"github.com/gofiber/fiber/v2/middleware/logger"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/recover"
	log "github.com/sirupsen/logrus"
)

const (
	errApp = iota
)

const (
	port = ":8080"
)

func main() {
	// App middlewares
	app := fiber.New()
	app.Use(recover.New())
	app.Use(logger.New())

	// Instantiate services
	sorter := sort.NewSorterService()
	courseService := course.NewService(sorter)

	// Routes
	app = handlers.CoursesRoutes(app, courseService)

	// Ready to go (rocket emoji)
	err := app.Listen(port)
	log.Fatal(err.Error())
	os.Exit(errApp)
}
